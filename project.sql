CREATE DATABASE  IF NOT EXISTS `project` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `project`;
-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: project
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `booking`
--

DROP TABLE IF EXISTS `booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `booking` (
  `booking_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `room_id` int DEFAULT NULL,
  `booking_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `check_in_date` date DEFAULT NULL,
  `check_out_date` date DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `status` varchar(50) DEFAULT 'Pending',
  `payment` varchar(50) DEFAULT 'Pending',
  PRIMARY KEY (`booking_id`),
  KEY `user_id` (`user_id`),
  KEY `room_id` (`room_id`),
  CONSTRAINT `booking_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `booking_ibfk_2` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`room_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking`
--

LOCK TABLES `booking` WRITE;
/*!40000 ALTER TABLE `booking` DISABLE KEYS */;
INSERT INTO `booking` VALUES (1,1,2,'2022-04-09 18:35:17','2022-03-26','2022-03-27','Need to book room for 2 people','Approved','Paid'),(2,2,4,'2022-04-09 18:35:17','2022-03-29','2022-03-30','Need to book room for 3 people','Approved','Paid'),(3,3,3,'2022-04-09 18:35:17','2022-03-31','2022-04-01','Need to book room for 2 people','Approved','Paid'),(4,4,1,'2022-04-09 18:35:17','2022-04-02','2022-04-04','Need to book room for single person','Approved','Paid'),(5,5,5,'2022-04-09 18:35:17','2022-04-10','2022-04-15','Need to book room for 4 people','Approved','Paid'),(6,6,6,'2022-04-09 18:35:17','2023-02-11','2023-02-15','Need to book room for 2 people','Approved','Paid'),(7,3,10,'2022-04-11 06:42:44','2022-04-11','2022-04-15','book my room ','approved','paid'),(8,NULL,1,'2022-04-22 06:50:18','2022-04-22','2022-04-24','book my room','approved','paid');
/*!40000 ALTER TABLE `booking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_us`
--

DROP TABLE IF EXISTS `contact_us`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contact_us` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `mobile_no` varchar(10) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_us`
--

LOCK TABLES `contact_us` WRITE;
/*!40000 ALTER TABLE `contact_us` DISABLE KEYS */;
INSERT INTO `contact_us` VALUES (1,'StarGatewayResortSystem','7038200015','A perfect place for vacation, relaxation or as a daytime getaway.Offering dining & an outdoor pool.','stargateway@test.com');
/*!40000 ALTER TABLE `contact_us` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enquiry`
--

DROP TABLE IF EXISTS `enquiry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `enquiry` (
  `enquiry_id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `mobile_no` varchar(10) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`enquiry_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enquiry`
--

LOCK TABLES `enquiry` WRITE;
/*!40000 ALTER TABLE `enquiry` DISABLE KEYS */;
INSERT INTO `enquiry` VALUES (2,'rahul','s','9850021565','rahuls@sunbeaminfo.com','looking for suite');
/*!40000 ALTER TABLE `enquiry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facility`
--

DROP TABLE IF EXISTS `facility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `facility` (
  `facility_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`facility_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facility`
--

LOCK TABLES `facility` WRITE;
/*!40000 ALTER TABLE `facility` DISABLE KEYS */;
INSERT INTO `facility` VALUES (1,'Breakfast','The hotel restaurant with both indoor and outdoor seating offers an open-buffet breakfast and is directly accessible from guestroom corridors.'),(2,'Internet Access','StarGateway Holiday Home & Suites offers free Internet access. High-speed wireless Internet connection is complimentary both in the rooms and public areas.'),(3,'Business Centre','StarGateway Holiday Home & Suites provides significant services for a flawless business-travel experience. The hotels business centre is designated to meet all your business travel needs and available to assist you with all office services such as scanning and emailing documents, fax services, photocopying, printing and offices supplies.'),(4,'Parking','StarGateway Holiday Home & Suites offers 24-hour indoor parking for its guests. The hotel is directly accessible from the indoor car park and guests can easily reach the shopping mall by using the elevators that are facing the car park hotel entrance. The indoor parking and valet services are free for accommodating hotel guests. Car washing service is also available with an additional cost.'),(5,'Relaxation Devices','After a long flight, a stressful day of meetings, or just the unpleasantness of jetlag, guests want to relax when they?re in a hotel room. Why not offer a little something extra to help your guests clear their minds? Think of what you like most about a spa: calming music, good aromatics, maybe even a massage. How can you replicate a spa-like experience in your guestrooms? Scent diffusers, soothing eye pillows, and noise machines are relatively inexpensive and easy to implement, so adding relaxation devices to your room amenities is certainly not a stressful task. If you want to deliver a truly unforgettable in-room relaxation station, consider adding a Myostorm massage ball, a unique product that was recently featured on Shark Tank. Other massage options include a foot massager or a neck massager, like these from Homedics.'),(6,'Bathrobes and slippers','Ready to take things to the next level? Many hotels now offer elevated amenities to better compete with vacation rentals. If your hotel is located in a market with a lot of competition from Airbnb - or if you?re just looking to grab a competitive edge - consider making these amenities available'),(7,'Gym or fitness center','Today?s guest doesn?t want to sacrifice their workout routine on the road, so even if your hotel doesn?t have its own fitness center, guests will appreciate discounted rates (or, even better, free classes or gym time) at a nearby fitness facility.'),(8,'Powerbank for the Road','Imagine you just got off a long flight, pulled up Google Maps to find your hotel, and completed mobile check-in. How terrible would it be to find that all that smartphone use drained your battery and you can?t take photos of your beautiful hotel room! Give travelers a much-appreciated (and unexpected!) amenity by offering power banks in the room or for rent at the front desk. As a bonus, you could encourage guests to post something about your hotel on social media in exchange for using a power bank! Or, for an over-the-top welcome gift, give guests a complimentary, hotel-branded power bank to take home.');
/*!40000 ALTER TABLE `facility` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `id_proof`
--

DROP TABLE IF EXISTS `id_proof`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `id_proof` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `id_proof`
--

LOCK TABLES `id_proof` WRITE;
/*!40000 ALTER TABLE `id_proof` DISABLE KEYS */;
INSERT INTO `id_proof` VALUES (1,'aadharCard'),(2,'voterId'),(3,'panCard'),(4,'drivingLicence');
/*!40000 ALTER TABLE `id_proof` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offers`
--

DROP TABLE IF EXISTS `offers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `offers` (
  `offer_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `time_period` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`offer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offers`
--

LOCK TABLES `offers` WRITE;
/*!40000 ALTER TABLE `offers` DISABLE KEYS */;
INSERT INTO `offers` VALUES (1,'Valentine Offer','Book your rooms with 50% instant discount','From 7th Feb to 14th Feb'),(2,'Summer Vacation Offer','Book your rooms with 30% instant discount','From 15th Apr to 15th May'),(3,'Credit card Offer','Book your rooms using credit crad  and get 30% instant discount up to 300 RS once per user','From 1st Apr to 30th June'),(4,'Citi Credit & debit card Offer','Book your rooms using citi credit/debit card and get 10% instant discount up to 1000 RS once per user, *T&C Applied','For Every Weekend bookings');
/*!40000 ALTER TABLE `offers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_category`
--

DROP TABLE IF EXISTS `room_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `room_category` (
  `category_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_category`
--

LOCK TABLES `room_category` WRITE;
/*!40000 ALTER TABLE `room_category` DISABLE KEYS */;
INSERT INTO `room_category` VALUES (1,'Single Room','A single room has one single bed for single occupancy. An additional bed (called extra bed) may be added to this room at the request of a guest and charged accordingly. The size of the bed is normally 3 feet by 6 feet. Mostly, the charge for a single room is occupied by one person.'),(2,'Twin Room','A twin room has two single beds for double occupancy. An extra bed may be added to this room at the request of a guest and charged accordingly. Here the bed size is normally 3 feet by 6 feet. These rooms are suitable for sharing accommodation among a group of delegates of meeting.'),(3,'Double Room','A double room has one double bed for double occupancy. An extra bed may be added to this room at the request of a guest and charged accordingly. The size of the double bed is generally 4.5 feet by 6 feet.'),(4,'TRIPLE ROOM','A triple room has three separate single beds and can be occupied by three guests. This type of room is suitable for groups and delegates of meetings and conferences.'),(5,'QUAD ROOM','A quad room has four separate single beds and can accommodate four persons together in the same room.'),(6,'HOLLYWOOD TWIN ROOM','A Hollywood twin room has two single beds with a common headboard. This hotel room type is generally occupied by two guests.'),(7,'DOUBLE-DOUBLE ROOM','A double-double room has two double beds and is normally preferred by a family or group as it can accommodate four persons together.'),(8,'KING ROOM','A king room has a king-size bed. The size of the bed is 6 feet by 6 feet. An extra bed may be added to this room at the request of a guest and charged accordingly.'),(9,'QUEEN ROOM','A queen room has a queen-size bed. The size of the bed is 5 feet by 6 feet. An extra bed may be added to this room at the request of a guest and charged accordingly.'),(10,'INTERCONNECTING ROOM','Interconnecting rooms have a common wall and a door that connects the two rooms. This allows guests to access any of the two rooms without passing through a public area. This type of hotel room is ideal for families and crew members in a 5-star hotel.'),(11,'CABANA ROOM','A Cabana is situated away from the main hotel building, in the vicinity of a swimming pool or sea beach. It may or may not have beds and is generally used as a changing room and not as a bedroom.'),(12,'SUITE ROOM','A Suite room is comprised of more than one room. Occasionally, it can also be a single large room with clearly defined sleeping and sitting areas. The decor of such units is of very high standards, aimed to please the affluent guest who can afford the high tariffs of the room category.'),(13,'DUPLEX ROOM','The duplex suite comprises two rooms situated on different floors, which are connected by an internal staircase. This suite is generally used by business guests who wish to use the lower level as an office and meeting place and the upper-level room as a bedroom. This type of room is quite expensive and only can be found in luxury hotels.'),(14,'HOSPITALITY ROOM','A hospitality room is designed for hotel guests who would want to entertain their own guests outside their allotted rooms. Such rooms are generally charged on an hourly basis.');
/*!40000 ALTER TABLE `room_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_facility`
--

DROP TABLE IF EXISTS `room_facility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `room_facility` (
  `room_id` int DEFAULT NULL,
  `facility_id` int DEFAULT NULL,
  KEY `facility_id` (`facility_id`),
  KEY `room_id` (`room_id`),
  CONSTRAINT `room_facility_ibfk_1` FOREIGN KEY (`facility_id`) REFERENCES `facility` (`facility_id`),
  CONSTRAINT `room_facility_ibfk_2` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_facility`
--

LOCK TABLES `room_facility` WRITE;
/*!40000 ALTER TABLE `room_facility` DISABLE KEYS */;
INSERT INTO `room_facility` VALUES (1,1),(2,2),(3,3),(4,4),(1,5),(2,6),(3,7),(5,1),(6,2),(7,3),(1,3),(9,1),(9,2),(9,3),(9,4),(10,1),(10,2),(10,8);
/*!40000 ALTER TABLE `room_facility` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rooms` (
  `room_id` int NOT NULL AUTO_INCREMENT,
  `category_id` int DEFAULT '1',
  `bed_count` int DEFAULT NULL,
  `adult_count` int DEFAULT NULL,
  `child_count` int DEFAULT NULL,
  `price` int DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`room_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `rooms_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `room_category` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms`
--

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` VALUES (1,1,1,1,0,800,'A single room has one single bed for single occupancy. An additional bed (called extra bed) may be added to this room at the request of a guest and charged accordingly. The size of the bed is normally 3 feet by 6 feet. Mostly, the charge for a single room is occupied by one person',NULL),(2,2,2,2,0,1100,'A twin room has two single beds for double occupancy. An extra bed may be added to this room at the request of a guest and charged accordingly. Here the bed size is normally 3 feet by 6 feet. These rooms are suitable for sharing accommodation among a group of delegates of meeting.',NULL),(3,3,1,2,0,1200,'A double room has one double bed for double occupancy. An extra bed may be added to this room at the request of a guest and charged accordingly. The size of the double bed is generally 4.5 feet by 6 feet.','./../../../image/rooms/1.jpg'),(4,4,3,2,1,1500,'A triple room has three separate single beds and can be occupied by three guests. This type of room is suitable for groups and delegates of meetings and conferences.','./../../../image/rooms/2.jpg'),(5,5,4,2,2,1800,'A quad room has four separate single beds and can accommodate four persons together in the same room.','./../../../image/rooms/3.jpg'),(6,6,2,1,1,2200,'A Hollywood twin room has two single beds with a common headboard. This hotel room type is generally occupied by two guests.','./../../../image/rooms/4.jpg'),(7,7,2,4,0,2500,'A double-double room has two double beds and is normally preferred by a family or group as it can accommodate four persons together.','./../../../image/rooms/5.jpg'),(8,8,2,2,0,3000,'A king room has a king-size bed. The size of the bed is 6 feet by 6 feet. An extra bed may be added to this room at the request of a guest and charged accordingly.','./../../../image/rooms/6.jpg'),(9,9,2,2,0,2500,'A queen room has a queen-size bed. The size of the bed is 5 feet by 6 feet. An extra bed may be added to this room at the request of a guest and charged accordingly.',NULL),(10,3,4,2,2,1900,'this is new room',NULL);
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `idproof` int DEFAULT '1',
  `created_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `gender` varchar(20) DEFAULT NULL,
  `role` varchar(20) DEFAULT 'user',
  `mobile` varchar(10) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `zipcode` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`),
  KEY `idproof` (`idproof`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`idproof`) REFERENCES `id_proof` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Nikhil','Kotkar','nikhil@test.com','$2a$10$.J1uaWgH4g9o.kViXNq0POfNAuWjDeCjRSAwNlYK0j9qaGwpEmWOa',2,'2022-04-09 18:35:16','male','admin','1234567890','Pimpri','Pune','Maharashtra','411019'),(2,'Pravin','Kumbhar','pravin@test.com','$2a$10$.J1uaWgH4g9o.kViXNq0POfNAuWjDeCjRSAwNlYK0j9qaGwpEmWOa',3,'2022-04-09 18:35:16','male','admin','1234543234','Bhavanipeth','Solapur','Maharashtra','413007'),(3,'Sandeep','Khamkar','sandy@test.com','$2a$10$.J1uaWgH4g9o.kViXNq0POfNAuWjDeCjRSAwNlYK0j9qaGwpEmWOa',4,'2022-04-09 18:35:16','male','user','7038200015','Nilanga','Latur','Maharashtra','413521'),(4,'Akshay','Kumar','akshay@test.com','$2a$10$.J1uaWgH4g9o.kViXNq0POfNAuWjDeCjRSAwNlYK0j9qaGwpEmWOa',2,'2022-04-09 18:35:16','male','user','7038201234','Juhu','Mumbai','Maharashtra','500001'),(5,'Virat','Kohli','virat@test.com','$2a$10$.J1uaWgH4g9o.kViXNq0POfNAuWjDeCjRSAwNlYK0j9qaGwpEmWOa',3,'2022-04-09 18:35:16','male','user','1234567890','Noida','Delhi','Delhi','110001'),(6,'Rohit','Sharma','rohit@test.com','$2a$10$.J1uaWgH4g9o.kViXNq0POfNAuWjDeCjRSAwNlYK0j9qaGwpEmWOa',1,'2022-04-09 18:35:16','male','user','7038204534','Dadar','Mumbai','Maharashtra','500007'),(7,'Sundar ','Pichai','sundar@test.com','$2a$10$.J1uaWgH4g9o.kViXNq0POfNAuWjDeCjRSAwNlYK0j9qaGwpEmWOa',2,'2022-04-09 18:35:16','male','user','9038201564','Madurai','Madurai','Tamil Nadu','625001'),(8,'Steve','Jobs','steve@test.com','$2a$10$.J1uaWgH4g9o.kViXNq0POfNAuWjDeCjRSAwNlYK0j9qaGwpEmWOa',4,'2022-04-09 18:35:16','male','user','7358891234','SanFrancisco','California','USA','940161');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-30 10:54:57
